//
//  Weaher+Description.swift
//  TestPractice
//
//  Created by HMC7342965 on 2023/04/26.
//

import Foundation

extension WeatherError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .networkError:
            return "서버에 요청을 보낼 수 없습니다. 네트워크 상태를 확인해 주세요."
        case .imageLoadError:
            return "서버에서 날씨 이미지를 가져오지 못했습니다."
        case .httpStatusCodeError:
            return "서버로부터 성공적인 응답을 받지 못했습니다."
        case .decodingError:
            return "서버에서 받은 데이터를 변환하지 못했습니다."
        case .emptyDataError:
            return "서버로부터 데이터를 받지 못했습니다."
        case .loadWithoutQueryError:
            return "선택된 도시가 없습니다."
        }
    }
}
