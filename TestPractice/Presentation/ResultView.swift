//
//  ResultView.swift
//  TestPractice
//
//  Created by HMC7342965 on 2023/04/21.
//

import UIKit

class ResultView: UIView {
    
    private let marginConstant: CGFloat = CGFloat(10)
    private let insetConstant = CGFloat(10)
    
    // MARK: 폴더 구조도 잘 정리하기 (clean architecture 기준 -나중에)
    
    private var cityLabel: UILabel = {
        var label = UILabel()
        label.textAlignment = .center
        label.textColor = .black
        return label
    }()
    
    private var weatherDiscriptionLabel: UILabel = {
        var label = UILabel()
        label.textAlignment = .center
        label.textColor = .black
        return label
    }()
    
    private var weatherImageView: UIImageView = {
        var imageView = UIImageView()
        return imageView
    }()
    
    private var currentTempLabel: UILabel = {
        var label = UILabel()
        label.textAlignment = .center
        label.textColor = .black
        return label
    }()
    
    private var feelTempLabel: UILabel = {
        var label = UILabel()
        label.textAlignment = .center
        label.textColor = .black
        return label
    }()
    
    private var maxTempLabel: UILabel = {
        var label = UILabel()
        label.textAlignment = .center
        label.textColor = .black
        return label
    }()
    
    private var minTempLabel: UILabel = {
        var label = UILabel()
        label.textAlignment = .center
        label.textColor = .black
        return label
    }()
    
    private var humidityLabel: UILabel = {
        var label = UILabel()
        label.textAlignment = .center
        return label
    }()
    
    private var windSpeedLabel: UILabel = {
        var label = UILabel()
        label.textAlignment = .center
        label.textColor = .black
        return label
    }()
    
    private var cloudsLabel: UILabel = {
        var label = UILabel()
        label.textAlignment = .center
        label.textColor = .black
        return label
    }()
    
    func setResultViewText(by weatherResultState: WeatherResultTextState) {
        cityLabel.text = weatherResultState.city
        weatherDiscriptionLabel.text = weatherResultState.description
        currentTempLabel.text = "현재 온도 : \(weatherResultState.currentTemp)℃"
        feelTempLabel.text = "체감 온도 : \(weatherResultState.feelsTemp)℃"
        minTempLabel.text = "최저 기온 : \(weatherResultState.minTemp)℃"
        maxTempLabel.text = "최고 기온 : \(weatherResultState.maxTemp)℃"
        humidityLabel.text = "습도 \(weatherResultState.humidity)%"
        windSpeedLabel.text = "풍속 \(weatherResultState.windSpeed)미터/초"
        cloudsLabel.text = "구름 \(weatherResultState.clouds)%"
    }
    
    func setResultViewImage(by image: UIImage) {
        weatherImageView.image = image
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupViews()
        setupConstraints()
    }
    
    private func setupViews() {
        self.addSubview(cityLabel)
        self.addSubview(weatherDiscriptionLabel)
        self.addSubview(weatherImageView)
        self.addSubview(currentTempLabel)
        self.addSubview(feelTempLabel)
        self.addSubview(maxTempLabel)
        self.addSubview(minTempLabel)
        self.addSubview(humidityLabel)
        self.addSubview(windSpeedLabel)
        self.addSubview(cloudsLabel)
    }
    
    private func setupConstraints() {
        cityLabel.translatesAutoresizingMaskIntoConstraints = false
        weatherDiscriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        weatherImageView.translatesAutoresizingMaskIntoConstraints = false
        currentTempLabel.translatesAutoresizingMaskIntoConstraints = false
        feelTempLabel.translatesAutoresizingMaskIntoConstraints = false
        maxTempLabel.translatesAutoresizingMaskIntoConstraints = false
        minTempLabel.translatesAutoresizingMaskIntoConstraints = false
        humidityLabel.translatesAutoresizingMaskIntoConstraints = false
        windSpeedLabel.translatesAutoresizingMaskIntoConstraints = false
        cloudsLabel.translatesAutoresizingMaskIntoConstraints = false
        
        
        NSLayoutConstraint.activate([
            cityLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: marginConstant),
            cityLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: marginConstant),
            cityLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -marginConstant),
            cityLabel.heightAnchor.constraint(equalToConstant: 50),
            
            weatherDiscriptionLabel.topAnchor.constraint(equalTo: cityLabel.bottomAnchor, constant: insetConstant),
            weatherDiscriptionLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: marginConstant),
            weatherDiscriptionLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -marginConstant),
            weatherDiscriptionLabel.heightAnchor.constraint(equalToConstant: 50),
            
            weatherImageView.topAnchor.constraint(equalTo: weatherDiscriptionLabel.bottomAnchor, constant: insetConstant),
            weatherImageView.widthAnchor.constraint(equalToConstant: 300),
            weatherImageView.heightAnchor.constraint(equalToConstant: 300),
            weatherImageView.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            
            currentTempLabel.topAnchor.constraint(equalTo: weatherImageView.bottomAnchor, constant: insetConstant),
            currentTempLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: marginConstant),
            currentTempLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -marginConstant),
            
            feelTempLabel.topAnchor.constraint(equalTo: currentTempLabel.bottomAnchor, constant: insetConstant),
            feelTempLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: marginConstant),
            feelTempLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -marginConstant),
            feelTempLabel.heightAnchor.constraint(equalTo: currentTempLabel.heightAnchor),
            
            maxTempLabel.topAnchor.constraint(equalTo: feelTempLabel.bottomAnchor, constant: insetConstant),
            maxTempLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: marginConstant),
            maxTempLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -marginConstant),
            maxTempLabel.heightAnchor.constraint(equalTo: currentTempLabel.heightAnchor),
            
            minTempLabel.topAnchor.constraint(equalTo: maxTempLabel.bottomAnchor, constant: insetConstant),
            minTempLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: marginConstant),
            minTempLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -marginConstant),
            minTempLabel.heightAnchor.constraint(equalTo: currentTempLabel.heightAnchor),
            
            humidityLabel.heightAnchor.constraint(equalToConstant: 50),
            humidityLabel.widthAnchor.constraint(equalTo: windSpeedLabel.widthAnchor, constant: insetConstant),
            humidityLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: marginConstant),
            humidityLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -marginConstant),
            
            windSpeedLabel.heightAnchor.constraint(equalToConstant: 50),
            windSpeedLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            windSpeedLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -marginConstant),
            
            cloudsLabel.heightAnchor.constraint(equalToConstant: 50),
            cloudsLabel.widthAnchor.constraint(equalTo: windSpeedLabel.widthAnchor, constant: insetConstant),
            cloudsLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -marginConstant),
            cloudsLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -marginConstant),
        ])
    }
}
