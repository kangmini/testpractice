//
//  ViewController.swift
//  TestPractice
//
//  Created by HMC7342965 on 2023/04/20.
//

import UIKit

import ReactorKit
import RxSwift
import RxCocoa

// View : 화면을 그리고, 관련 데이터와 바인딩을 한다. Action을 방출한다.
class WeatherViewController: UIViewController, View {
    
    typealias Reactor = WeatherViewReactor
    
    var disposeBag = DisposeBag()
    private let cityList = City.allCases.map { "\($0.rawValue)" }
    
    private let searchView = SearchView()
    private let resultView = ResultView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .white
        
        searchView.cityPickerView.delegate = self
        searchView.cityPickerView.dataSource = self
        
        searchView.cityTextField.delegate = self
        
        setupViews()
        setupConstraints()
    }
    
    func bind(reactor: WeatherViewReactor) {
        bindAction(reactor: reactor)
        bindState(reactor: reactor)
    }
    
    private func bindAction(reactor: WeatherViewReactor) {
        // action (View -> Reactor) 리액터에 요청할 작업
        searchView.searchButton.rx.tap
            .map { [weak self] in
                guard let query = self?.searchView.cityTextField.text,
                      !query.isEmpty
                else {
                    return Reactor.Action.tapLoadWeatherWithoutQuery
                }
                
                return Reactor.Action.tapLoadWeather(query)
            }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        
        reactor.state
            .map { $0.weatherResultState.imageId }
            .unwrap()
            .distinctUntilChanged()
            .observe(on: MainScheduler.asyncInstance)
            .map { Reactor.Action.loadImage($0) }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
    }
    
    private func bindState(reactor: WeatherViewReactor) { // state (Reactor -> View)
        reactor.state.map { $0.weatherResultState }
            .distinctUntilChanged()
            .observe(on: MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] weatherResultTextState in
                self?.resultView.setResultViewText(by: weatherResultTextState)
            })
            .disposed(by: disposeBag)
        
        reactor.state.map { $0.image }
            .distinctUntilChanged()
            .map { UIImage(data: $0) }
            .unwrap()
            .observe(on: MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] image in
                self?.resultView.setResultViewImage(by: image)
            })
            .disposed(by: disposeBag)
        
        reactor.state
            .map { $0.isLoading }
            .distinctUntilChanged()
            .filter { $0 == false }
            .observe(on: MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] _ in
                self?.searchView.cityTextField.resignFirstResponder()
            })
            .disposed(by: disposeBag)
        
        reactor.state
            .map{ $0.isTouchError }
            .distinctUntilChanged()
            .filter { $0 == true }
            .observe(on: MainScheduler.asyncInstance)
            .subscribe(onNext : { [weak self] _ in
                guard let alert = self?.buildNoticeAlert(error: WeatherError.loadWithoutQueryError)
                else { return }
                self?.present(alert, animated: true)
            })
            .disposed(by: disposeBag)
        
        reactor.state
            .map { $0.error }
            .distinctUntilChanged()
            .observe(on: MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] weatherError in
                guard let weatherError = weatherError,
                      let alert = self?.buildErrorAlert(error: weatherError)
                else { return }
                self?.present(alert, animated: true)
            }).disposed(by: disposeBag)
    }
    
    private func setupViews() {
        self.view.addSubview(searchView)
        self.view.addSubview(resultView)
    }
    
    private func setupConstraints() {
        searchView.translatesAutoresizingMaskIntoConstraints = false
        resultView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            searchView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor),
            searchView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor),
            searchView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor),
            searchView.heightAnchor.constraint(equalToConstant: 100),
            
            resultView.topAnchor.constraint(equalTo: searchView.bottomAnchor),
            resultView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor),
            resultView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor),
            resultView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor),
        ])
    }
    
    private func buildNoticeAlert(error: WeatherError) -> UIAlertController {
        let alert = UIAlertController(title: "알림",
                                      message: error.errorDescription,
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "확인",
                                      style: .default))
        return alert
    }
    
    private func buildErrorAlert(error: WeatherError) -> UIAlertController {
        let alert = UIAlertController(title: "문제 발생",
                                      message: error.errorDescription,
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "확인",
                                      style: .default))
        return alert
    }
}

extension WeatherViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return cityList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return cityList[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        searchView.cityTextField.text = cityList[row]    }
}

extension WeatherViewController: UITextFieldDelegate {
    // 사용자가 textField의 텍스트를 바꿀 수 있는지에 대한 설정
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return false
    }
}

