//
//  ResultViewData.swift
//  TestPractice
//
//  Created by HMC7342965 on 2023/04/26.
//

import Foundation

struct WeatherResultTextState: Equatable {
    
    static let empty = WeatherResultTextState()
    
    var city: String
    var imageId: String?
    var description: String
    var currentTemp: Double
    var feelsTemp: Double
    var minTemp: Double
    var maxTemp: Double
    var humidity: Int
    var windSpeed: Double
    var clouds: Int
    
    init(city: String = "",
         imageId: String? = nil,
         description: String = "",
         currentTemp: Double = 0.0,
         feelsTemp: Double = 0.0,
         minTemp: Double = 0.0,
         maxTemp: Double = 0.0,
         humidity: Int = 0,
         windSpeed: Double = 0.0,
         clouds: Int = 0
    ) {
        self.city = city
        self.imageId = imageId
        self.description = description
        self.currentTemp = currentTemp
        self.feelsTemp = feelsTemp
        self.minTemp = minTemp
        self.maxTemp = maxTemp
        self.humidity = humidity
        self.windSpeed = windSpeed
        self.clouds = clouds
    }
}
