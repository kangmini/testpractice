//
//  SearchView.swift
//  TestPractice
//
//  Created by HMC7342965 on 2023/04/21.
//

import UIKit

class SearchView: UIView {
    
    private let marginConstant: CGFloat = 10
    
    private let askLabel: UILabel = {
        var label = UILabel()
        label.text = "🌞날씨를 알아봐요🌝"
        label.textAlignment = .center
        return label
    }()
    
    let cityTextField: UITextField = {
        var textField = UITextField()
        textField.backgroundColor = .systemGray3
        textField.tintColor = .clear // 커서 색상
        textField.placeholder = "도시 선택"
        return textField
    }()
    
    let cityPickerView: UIPickerView = {
        var pickerView = UIPickerView()
        return pickerView
    }()
    
    let searchButton: UIButton = {
        var button = UIButton()
        var config = UIButton.Configuration.filled()
        config.title = "조회"
        button.configuration = config
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupViews()
        setupConstraints()
    }
    
    private func setupViews() {
        addSubview(askLabel)
        addSubview(searchButton)
        addSubview(cityTextField)
        cityTextField.inputView = cityPickerView
    }
    
    private func setupConstraints() {
        askLabel.translatesAutoresizingMaskIntoConstraints = false
        searchButton.translatesAutoresizingMaskIntoConstraints = false
        cityTextField.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            askLabel.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor),
            askLabel.leadingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leadingAnchor, constant: marginConstant),
            askLabel.trailingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.trailingAnchor, constant: marginConstant),
            askLabel.heightAnchor.constraint(equalToConstant: 50),
            
            searchButton.topAnchor.constraint(equalTo: askLabel.bottomAnchor),
            searchButton.trailingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.trailingAnchor, constant: -marginConstant),
            searchButton.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor),
            searchButton.widthAnchor.constraint(equalToConstant: 80),
            
            cityTextField.topAnchor.constraint(equalTo: askLabel.bottomAnchor),
            cityTextField.leadingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leadingAnchor, constant: marginConstant),
            cityTextField.trailingAnchor.constraint(equalTo: searchButton.leadingAnchor, constant: -marginConstant),
            cityTextField.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor),
        ])
    }
}
