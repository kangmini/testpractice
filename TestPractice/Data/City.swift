//
//  City.swift
//  TestPractice
//
//  Created by HMC7342965 on 2023/04/21.
//

import Foundation

enum City: String, CaseIterable {
    case Seoul
    case Busan
    case Incheon
    case Daegu
    case Daejeon
    case Gwangju
    case Ulsan
    case Suwon
    case Yongin
    case Changwon
    case Jeonju
    case Cheonan
    case Pohang
//    case Goyang = "고양" // "Goyang-si"
//    case Seongnam = "성남" // "Seongnam-si"
//    case Cheongju = "청주" // "Cheongju-si"
//    case Jeju = "제주" // "Jeju-do"
}
