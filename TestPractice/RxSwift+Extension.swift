//
//  RxSwift+Extension.swift
//  TestPractice
//
//  Created by HMC7342965 on 2023/04/25.
//

import Foundation
import RxSwift

extension ObservableType {

 

    /**

     Takes a sequence of optional elements and returns a sequence of non-optional elements, filtering out any nil values.

 

     - returns: An observable sequence of non-optional elements

     */

 

    public func unwrap<Result>() -> Observable<Result> where Element == Result? {

        return self.compactMap { $0 }

    }

}
