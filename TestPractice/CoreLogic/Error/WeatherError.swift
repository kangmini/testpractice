//
//  WeatherError.swift
//  TestPractice
//
//  Created by HMC7342965 on 2023/04/25.
//

import Foundation

enum WeatherError: Error, Equatable {
    case networkError
    case httpStatusCodeError
    case emptyDataError
    case decodingError
    case imageLoadError
    case loadWithoutQueryError
}
