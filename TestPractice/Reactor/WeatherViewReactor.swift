//
//  SearchViewReactor.swift
//  TestPractice
//
//  Created by HMC7342965 on 2023/04/20.
//
import Foundation

import ReactorKit
import RxSwift
import RxCocoa

// Reactor : UI 독립적이며 뷰의 상태를 관리함. 뷰에서 제어 흐름(로직)을 분리함. State를 방출함.
class WeatherViewReactor: Reactor {
    
    let initialState: State
    
    init() {
        self.initialState = State(
            isLoading: false,
            isTouchError: false,
            image: Data(),
            weatherResultState: WeatherResultTextState.empty
        )
    }
    
    enum Action {
        case tapLoadWeather(String)
        case tapLoadWeatherWithoutQuery
        case loadImage(String)
    }
    
    enum Mutation {
        case weatherLoaded(WeatherResult)
        case imageLoaded(Data)
        case setLoading(Bool)
        case wrongLoadRequest(Bool)
        case error(WeatherError?)
        
    }
    
    struct State {
        var isLoading: Bool
        var isTouchError: Bool
        var error: WeatherError?
        var image: Data
        var weatherResultState: WeatherResultTextState
    }
    
    func mutate(action: Action) -> Observable<Mutation> {
        switch action {
        case .tapLoadWeather(let query):
            return Observable.concat([
                Observable.just(Mutation.setLoading(true)),
                loadWeatherMutation(query: query),
                Observable.just(Mutation.setLoading(false))
            ])
        case .loadImage(let imageId):
            return Observable.concat([
                Observable.just(Mutation.setLoading(true)),
                loadImageMutation(query: imageId),
                Observable.just(Mutation.setLoading(false))
            ])
        case .tapLoadWeatherWithoutQuery:
            return Observable.concat([
                Observable.just(Mutation.wrongLoadRequest(true)),
                Observable.just(Mutation.wrongLoadRequest(false))
            ])
        }
    }
    
    // reduce() : 이전 State와 Mutation으로부터 새로운 State를 생성함. 순수함수, 동기로 새로운 State 반환.
    func reduce(state: State, mutation: Mutation) -> State {
        var newState = state
        newState.isLoading = false
        newState.isTouchError = false
        newState.error = nil
        
        switch mutation {
        case .weatherLoaded(let weatherResult):
            newState.weatherResultState.city = weatherResult.name
            newState.weatherResultState.imageId = weatherResult.weather.first?.icon ?? ""
            newState.weatherResultState.description = weatherResult.weather.first?.description ?? ""
            newState.weatherResultState.currentTemp = weatherResult.main.temp
            newState.weatherResultState.feelsTemp = weatherResult.main.feelsLike
            newState.weatherResultState.minTemp = weatherResult.main.tempMin
            newState.weatherResultState.maxTemp = weatherResult.main.tempMax
            newState.weatherResultState.humidity = weatherResult.main.humidity
            newState.weatherResultState.windSpeed = weatherResult.wind.speed
            newState.weatherResultState.clouds = weatherResult.clouds.all
            
        case .imageLoaded(let data):
            newState.image = data
            
        case .setLoading(let isLoading):
            newState.isLoading = isLoading
            
        case .wrongLoadRequest(let bool):
            if bool {
                newState.isTouchError = bool
            }
            
        case .error(let error):
            newState.error = error
        }
        
        return newState
    }
    
    private func loadWeatherMutation(query: String) -> Observable<Mutation> {
        self.loadWeatherFromAPI(query: query)
            .map {
                Mutation.weatherLoaded($0)
                // MARK: API 요청2개 붙이기 - icon이 있으면 이미지 요청. 구글링해서 알아보기
            }
            .catch({ error in
                let weatherError = error as? WeatherError
                return Observable.just(Mutation.error(weatherError))
            })
    }
    
    private func loadImageMutation(query: String) -> Observable<Mutation> {
        self.loadImageFromAPI(imageID: query)
            .map { Mutation.imageLoaded($0) }
            .catch({ error in
                Observable.just(Mutation.error(.imageLoadError))
            })
    }
    
    private func loadWeatherFromAPI(query: String) -> Observable<WeatherResult> {
        let emptyResult = WeatherResult()
        // MARK: url 주소 부분 분리
        guard let url = URL(string:
                                "https://api.openweathermap.org/data/2.5/weather?q=\(query)&appid=9ff6551b3faec5ed76e7a4da408fb64a&units=metric&lang=kr")
        else { return .just(emptyResult) }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        return Observable<WeatherResult>.create { observer in
            URLSession.shared.dataTask(with: request) { data, response, error in
                if let _ = error {
                    observer.onError(WeatherError.networkError)
                    return
                }
                
                guard let data = data
                else {
                    observer.onError(WeatherError.emptyDataError)
                    return
                }
                
                if let response = response as? HTTPURLResponse {
                    if !(200..<300).contains(response.statusCode) {
                        observer.onError(WeatherError.httpStatusCodeError)
                        return
                    }
                }
                
                guard let weatherResult = try? JSONDecoder().decode(WeatherResult.self, from: data)
                else {
                    observer.onError(WeatherError.decodingError)
                    return
                }
                
                observer.onNext(weatherResult)
                observer.onCompleted()
            }.resume()
            
            return Disposables.create()
        }
    }
    
    private func loadImageFromAPI(imageID: String) -> Observable<Data> {
        let emptyResult = Data()
        
        guard let url = URL(string: "https://openweathermap.org/img/wn/\(imageID)@2x.png")
        else { return .just(emptyResult) }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        return Observable<Data>.create { observer in
            URLSession.shared.dataTask(with: request) { data, response, error in
                if error != nil {
                    observer.onError(WeatherError.networkError)
                    return
                }
                
                if let response = response as? HTTPURLResponse {
                    if !(200..<300).contains(response.statusCode) {
                        observer.onError(WeatherError.httpStatusCodeError)
                        return
                    }
                }
                
                guard let data = data else {
                    observer.onError(WeatherError.emptyDataError)
                    return
                }
                
                observer.onNext(data)
                observer.onCompleted()
            }.resume()
            
            return Disposables.create()
        }
    }
}
