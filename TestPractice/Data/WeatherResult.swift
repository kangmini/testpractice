//
//  Weather.swift
//  TestPractice
//
//  Created by HMC7342965 on 2023/04/22.
//

import Foundation

struct WeatherResult: Decodable {
    let weather: [Weather]
    let main: Main
    let wind: Wind
    let clouds: Clouds
    let name: String
    
    init(weather: [Weather], main: Main, wind: Wind, clouds: Clouds, name: String) {
        self.weather = weather
        self.main = main
        self.wind = wind
        self.clouds = clouds
        self.name = name
    }
    
    init() {
        self.weather = []
        self.main = Main()
        self.wind = Wind()
        self.clouds = Clouds()
        self.name = ""
    }
}

struct Weather: Decodable {
    let description: String
    let icon: String
    
    init(description: String = "", icon: String = "") {
        self.description = description
        self.icon = icon
    }
}

struct Main: Decodable {
    let temp: Double
    let feelsLike: Double
    let tempMin: Double
    let tempMax: Double
    let humidity: Int
    
    enum CodingKeys: String, CodingKey {
        case temp
        case feelsLike = "feels_like"
        case tempMin = "temp_min"
        case tempMax = "temp_max"
        case humidity
    }
    
    init(temp: Double = 0.0, feelsLike: Double = 0.0, tempMin: Double = 0.0, tempMax: Double = 0.0, humidity: Int = 0) {
        self.temp = temp
        self.feelsLike = feelsLike
        self.tempMin = tempMin
        self.tempMax = tempMax
        self.humidity = humidity
    }
}

struct Wind: Decodable {
    let speed: Double
    
    init(speed: Double = 0.0) {
        self.speed = speed
    }
}

struct Clouds: Decodable {
    let all: Int
    
    init(all: Int = 0) {
        self.all = all
    }
}
